const express = require('express')
const fs = require("fs");
const app = express()

app.use(express.json())
app.use(express.static(__dirname));

app.get('/', (req, res) => {
    res.sendFile(`${__dirname}/index.html`)
})
app.get('/contacts', (req, res) => {
    fs.readFile("./contacts.json", "utf8", (err, jsonString) => {
        if (err) {
            console.log("File read failed:", err);
            return;
        }
        res.send(JSON.parse(jsonString))
    });
})

app.post('/api/submit', (req, res) => {
    let obj = req.body
    var contacts = []
    fs.readFile("./contacts.json", "utf8", (err, jsonString) => {
        if (err) {
            console.log("File read failed:", err);
            return;
        }
        let contacts = JSON.parse(jsonString)
        contacts.push(obj)
        fs.writeFile('./contacts.json', JSON.stringify(contacts), err => {
            if (err) {
                console.log('Error writing file', err)
            } else {
                res.send('Successfully wrote file')
            }
        })
    });
})

app.listen((8080), function () {
    console.log("running on 8080...")
})