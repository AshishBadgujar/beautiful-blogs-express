FROM node:13-alpine
WORKDIR /
COPY . .
EXPOSE 8080
CMD ["npm","start"]
